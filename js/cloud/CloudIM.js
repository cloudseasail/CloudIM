CloudIM = (function(){

	var ConvClass = Cloud.use( "_Conversation");
	
	function syncConversationList(id, done){
		var convs = [];
		var query = new AV.Query(ConvClass);
		query.equalTo("m", id);
		query.find().then(function(results) {
		  // Collect one promise for each delete into an array.
		  console.log(results.length + " conversation synced")
		  var promises = [];
		   
		    // 处理返回的结果数据
		    for (var i = 0; i < results.length; i++) {
		    	
		      var object = results[i];

		      var _conv ={
		      	conv_id:object.getObjectId(),
		      	m: object.get('m'),
		      	my_id: id,
		      	latestMsg: object.get('latestMsg') && JSON.parse(object.get('latestMsg')),
		      }
		      console.log("finding: "+ JSON.stringify(_conv))
		      var promise = getPeerUser(_conv, function(conv, peer){

		      	conv.peerUser = peer,
		      	console.log("find peer User: "+ JSON.stringify(conv))
		      	convs.push(conv);
		      })
		     
		      promises.push(promise);
		    }
		    
		  // Return a new promise that is resolved when all of the deletes are finished.
		  return AV.Promise.when(promises);
		
		},function(error){
			console.log("Error: " + error.code + " " + error.message);
		}).then(function() {
		  done&& done(convs)
		});
		
	
	}
	
	function getPeerUser(o, find){
		var peerIds = [];
		var members = o.m;
		for(var i in members){
			if(members[i] != o.my_id){
				peerIds.push(members[i])
			}
		}
		
		
		if(peerIds.length == 0){
			return null;
		}
		var peerId = peerIds[0];
		
		
		var query = new AV.Query(AV.User);
		query.equalTo("objectId", peerId);
//		query.equalTo("username", peerId); 
		return query.find({
			  success: function(peer) {
			  	peer = peer[0]
			  	console.log(JSON.stringify(peer))
			  	if(peer){
				  	var peerUser = {
				  		userName:　peer.get('username'),
				  		userId:　peer.getObjectId(),
				  		userPhoto: peer.get('photo'),
				  		pushId: peer.get('pushId'),
				  		
				  	}
				    find && find(o, peerUser);
				}
			  },
			  error: function(){
				
			  }
		});
				
	}
	
 
	
	
	return {
		sync: syncConversationList,
		
	}
	
}())
